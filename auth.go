package twitter

import (
	"context"
	"fmt"

	// "net/http"
	"regexp"
	"strings"
)

// auth.go - Business logic for handling user registration

var (
	UsernameMinLength = 2
	PasswordMinLength = 6
)

// accepted email regex
var emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// return from register - register method which returns result of register and any errors
type AuthService interface {
	Register(ctx context.Context, input RegisterInput) (AuthResponse, error)
}

// return token and user once logged in
type AuthResponse struct {
	AccessToken string
	User		User
}

// user inputs needed for registering a user
type RegisterInput struct {
	Email 			string
	Username 		string
	Password 		string
	ConfirmPassword string
}

// trim and remove any white space, for modifying RegisterInput struct
func (in *RegisterInput) Sanitize() {
	in.Email = strings.TrimSpace(in.Email)
	in.Email = strings.ToLower(in.Email)

	in.Username = strings.TrimSpace(in.Username)
}

// user validation
func (in RegisterInput) Validate() error {

	// @TODO - tidy up below with a func

	// return error if username is too short
	if len(in.Username) < UsernameMinLength {
		return fmt.Errorf("%w: username not long enough, (%d) characters at least", ErrValidation, UsernameMinLength)
	}

	// return error if email doesn't match regex
	if !emailRegexp.MatchString(in.Email) {
		return fmt.Errorf("%w: email not valid", ErrValidation)
	}

	// return error if password is too short
	if len(in.Password) < PasswordMinLength {
		return fmt.Errorf("%w: password not long enough (%d) characters at least", ErrValidation, PasswordMinLength)
	}

	// return error if passwords don't match
	if in.Password != in.ConfirmPassword {
		return fmt.Errorf("%w: confirm password must match the password", ErrValidation)
	}

	return nil // if we reach here return no errors
}