package twitter

import "errors"

// twitter.go - utils file for reusables

// generic error consts
var (
	ErrNotFound = errors.New("not found")
	ErrValidation = errors.New("Validation error")
)