package domain

import (
	"context"
	"testing"

	"github.com/aidan/twitter"
	"github.com/aidan/twitter/mocks"
	"github.com/stretchr/testify/require"
)

func TestAuthService_Register(t *testing.T) {
	ctx := context.Background()

	// valid input type
	validInput := twitter.RegisterInput{
		Username: 		    "bob",
		Email: 			    "bob@example.com",
		Password:			"password",
		ConfirmPassword:	"password",
	}
	t.Run("can register", func(t *testing.T) {
		userRepo := &mocks.UserRepo{}

		service := NewAuthService(userRepo)

		res, err := service.Register(ctx, validInput)
		require.NoError(t, err)

		// assertions
		require.NotEmpty(t, res.AccessToken)
		require.NotEmpty(t, res.User.ID)
		require.NotEmpty(t, res.User.Email)
		require.NotEmpty(t, res.User.Username)

		userRepo.AssertExpectations(t)
	})
}