package twitter

import (
	"context"
	"errors"
	"time"
)

// user.go/UserRepo - Business identity logic. In charge of calling db for handling crud operations

// login errors accessible through twitter package
var (
	ErrUsernameTaken = errors.New("username taken")
	ErrEmailTaken = errors.New("email taken")
)

// methods which return User type and error
type UserRepo interface {
	Create(ctx context.Context, user User) (User, error)
	GetByUsername(ctx context.Context, username string) (User, error)
	GetByEmail(ctx context.Context, email string) (User, error)
}

// user type
type User struct {
	ID 			string
	Username 	string
	Email 		string
	Password 	string
	CreatedAt 	time.Time
	UpdatedAt 	time.Time
}