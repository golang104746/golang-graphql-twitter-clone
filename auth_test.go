package twitter

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRegisterInput_Sanitize(t *testing.T) {
	// errors:
	input := RegisterInput{
		Username: 		 " bob ",
		Email: 			 " BOB@gmail.com  ",
		Password: 		 "password",
		ConfirmPassword: "password",
	}

	// expect:
	want := RegisterInput{
		Username: 		 "bob",
		Email: 			 "bob@gmail.com",
		Password: 		 "password",
		ConfirmPassword: "password",
	}

	input.Sanitize()

	require.Equal(t, want, input)
}

// table driving testing
func TestRegisterInput_Validate(t *testing.T) {
	// initialize a slice of struct type
	testCases := []struct {
		name string
		input RegisterInput
		err error
	}{
		{
			name: "valid",
			input: RegisterInput{
				Username: "bob",
				Email: "bob@gmail.com",
				Password: "password",
				ConfirmPassword: "password",
			},
			err: nil,
		},
		{
			name: "invalid email",
			input: RegisterInput{
				Username: "bob",
				Email: "bob",
				Password: "password",
				ConfirmPassword: "password",
			},
			err: ErrValidation,
		},
		{
			name: "too short username",
			input: RegisterInput{
				Username: "b",
				Email: "bob@gmail.com",
				Password: "password",
				ConfirmPassword: "password",
			},
			err: ErrValidation,
		},
		{
			name: "too short password",
			input: RegisterInput{
				Username: "bob",
				Email: "bob@gmail.com",
				Password: "pass",
				ConfirmPassword: "pass",
			},
			err: ErrValidation,
		},
		{
			name: "confirm password doesn't match password",
			input: RegisterInput{
				Username: "bob",
				Email: "bob@gmail.com",
				Password: "password",
				ConfirmPassword: "wrongpassword",
			},
			err: ErrValidation,
		},
	}
	// run each test case and check for errors
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.input.Validate()

			if tc.err != nil {
				// want to have an error
				// testing, error received and target - make sure err we get matches what we pass
				require.ErrorIs(t, err, tc.err)
			} else {
				// we don't have an error
				require.NoError(t, err)
			}
		})
	}
}